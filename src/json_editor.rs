use std::collections::HashMap;

use crossterm::event::{DisableMouseCapture, EnableMouseCapture, KeyEvent};
use crossterm::execute;
use crossterm::terminal::{enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen};
use ratatui::prelude::Backend;
use ratatui::Terminal;

use crate::tui::Runnable;

#[derive(Debug, Default)]
pub enum CurrentScreen {
    #[default]
    Main,
    Editing,
    Exiting,
}

#[derive(Debug)]
pub enum CurrentlyEditing {
    Key,
    Value,
}

#[derive(Debug, Default)]
pub struct App {
    pub key_input: String,
    pub value_input: String,
    pub pairs: HashMap<String, String>,
    pub current_screen: CurrentScreen,
    pub currently_editing: Option<CurrentlyEditing>,
}

impl<B: Backend + std::io::Write + Send + Sync + 'static> Runnable<B> for App {
    fn init(&mut self, terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        enable_raw_mode()?;

        execute!(
            terminal.backend_mut(),
            EnterAlternateScreen,
            EnableMouseCapture
        )?;
        Ok(())
    }

    fn run(&mut self, _terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        todo!()
    }

    fn restore(terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        execute!(
            terminal.backend_mut(),
            LeaveAlternateScreen,
            DisableMouseCapture
        )?;
        Ok(())
    }

    fn handle_key_event(&mut self, key_event: KeyEvent) -> color_eyre::Result<()> {
        todo!()
    }
}

impl App {
    pub fn new() -> Self {
        Self::default()
    }
}

// Logic

impl App {
    // save a key pair currently edited
    pub fn save_key_value(&mut self) {
        self.pairs
            .insert(self.key_input.clone(), self.value_input.clone());

        self.key_input = "".to_string();
        self.value_input = "".to_string();
        self.currently_editing = None;
    }

    pub fn toggle_editing(&mut self) {
        self.currently_editing = match self.currently_editing {
            Some(CurrentlyEditing::Key) => Some(CurrentlyEditing::Value),
            Some(CurrentlyEditing::Value) | None => Some(CurrentlyEditing::Key),
        }
    }

    pub fn print_json(&self) -> color_eyre::Result<()> {
        let output = serde_json::to_string(&self.pairs)?;
        println!("{}", output);
        Ok(())
    }
}
