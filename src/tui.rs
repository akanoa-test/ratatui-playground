use std::panic;

use color_eyre::config::HookBuilder;
use color_eyre::eyre;
use color_eyre::eyre::Context;
use crossterm::event;
use crossterm::event::{Event, KeyEvent, KeyEventKind};
use ratatui::backend::Backend;
use ratatui::Terminal;

pub trait Runnable<B>
where
    B: Backend + Sync + Send + 'static,
{
    fn init(&mut self, terminal: &mut Terminal<B>) -> color_eyre::Result<()>;
    fn run(&mut self, terminal: &mut Terminal<B>) -> color_eyre::Result<()>;

    fn restore(terminal: &mut Terminal<B>) -> color_eyre::Result<()>;

    fn handle_events(&mut self) -> color_eyre::Result<()> {
        match event::read()? {
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => self
                .handle_key_event(key_event)
                .wrap_err_with(|| format!("handling key event failed:\n{key_event:#?}"))?,
            _ => {}
        };
        Ok(())
    }
    fn handle_key_event(&mut self, key_event: KeyEvent) -> color_eyre::Result<()>;

    fn install_hooks(&mut self) -> color_eyre::Result<()>
    where
        Self: Send,
        Self: Sync,
    {
        let (panic_hook, eyre_hook) = HookBuilder::default().into_hooks();
        let panic_hook = panic_hook.into_panic_hook();

        panic::set_hook(Box::new(move |panic_info| {
            panic_hook(panic_info);
        }));

        let eyre_hook = eyre_hook.into_eyre_hook();
        eyre::set_hook(Box::new(
            move |error: &(dyn std::error::Error + 'static)| eyre_hook(error),
        ))?;

        Ok(())
    }
}
