use std::io::stdout;
use std::panic::{AssertUnwindSafe, catch_unwind};

use color_eyre::eyre::eyre;
use ratatui::backend::{Backend, CrosstermBackend};
use ratatui::Terminal;

use crate::tui::Runnable;

mod counter;
mod json_editor;
mod tui;

fn main() -> color_eyre::Result<()> {
    let backend = CrosstermBackend::new(stdout());
    let app = counter::App::default();
    run_app(backend, app)
}

fn run_app<B: Backend + Sync + Send + 'static, R: Runnable<B> + Send + Sync + 'static>(
    backend: B,
    mut app: R,
) -> color_eyre::Result<()> {
    let mut terminal = Terminal::new(backend)?;
    app.install_hooks()?;
    app.init(&mut terminal)?;
    let result = catch_unwind(AssertUnwindSafe(|| app.run(&mut terminal)));
    R::restore(&mut terminal)?;

    result.map_err(|_err| eyre!("Error on catch unwind"))?
}
