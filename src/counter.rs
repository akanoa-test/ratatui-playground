use color_eyre::eyre::{bail, Context};
use crossterm::event::{KeyCode, KeyEvent};
use crossterm::execute;
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use ratatui::{
    prelude::*,
    symbols::border,
    widgets::{block::*, *},
};

use crate::tui::Runnable;

#[derive(Debug, Default)]
pub struct App {
    counter: u8,
    exit: bool,
}

impl<B: Backend + std::io::Write + Send + Sync + 'static> Runnable<B> for App {
    fn init(&mut self, terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        execute!(terminal.backend_mut(), EnterAlternateScreen,)?;
        enable_raw_mode()?;
        Ok(())
    }

    fn run(&mut self, terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        while !self.exit {
            terminal.draw(|frame| self.render_frame(frame))?;
            <App as Runnable<B>>::handle_events(self).wrap_err("handle events failed")?;
        }
        Ok(())
    }

    fn restore(terminal: &mut Terminal<B>) -> color_eyre::Result<()> {
        execute!(terminal.backend_mut(), LeaveAlternateScreen,)?;
        disable_raw_mode()?;
        Ok(())
    }

    fn handle_key_event(&mut self, key_event: KeyEvent) -> color_eyre::Result<()> {
        match key_event.code {
            KeyCode::Char('q') => self.exit(),
            KeyCode::Right => self.incr(),
            KeyCode::Left => self.decr()?,
            _ => {}
        }
        Ok(())
    }
}

impl App {
    fn render_frame(&self, frame: &mut Frame) {
        frame.render_widget(self, frame.size())
    }
}

impl App {
    fn exit(&mut self) {
        self.exit = true
    }

    fn incr(&mut self) {
        self.counter += 1;
    }

    fn decr(&mut self) -> color_eyre::Result<()> {
        if self.counter < 1 {
            bail!("Can't decrease more");
        }
        self.counter -= 1;
        Ok(())
    }
}

impl Widget for &App {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let title = Title::from(" Counter App Tutorial ".bold());

        let instructions = Title::from(Line::from(vec![
            " Decrement ".into(),
            "<Left>".blue().bold(),
            " Increment ".into(),
            "<Right>".blue().bold(),
            " Quit ".into(),
            "<Q> ".blue().bold(),
        ]));

        let block = Block::default()
            .title(title.alignment(Alignment::Center))
            .title(
                instructions
                    .alignment(Alignment::Center)
                    .position(Position::Bottom),
            )
            .borders(Borders::ALL)
            .border_set(border::PLAIN);
        let counter_text = Text::from(vec![Line::from(vec![
            "Value: ".into(),
            self.counter.to_string().yellow(),
        ])]);
        Paragraph::new(counter_text)
            .centered()
            .block(block)
            .render(area, buf)
    }
}
